import {
	defineConfig,
	presetAttributify,
	presetTypography,
	presetUno,
	transformerDirectives,
	transformerVariantGroup,
} from "unocss";

export default defineConfig({
	presets: [presetAttributify(), presetUno(), presetTypography()],
	transformers: [transformerDirectives(), transformerVariantGroup()],
	theme: {
		fontFamily: {
			sans: ["Poppins", "ui-serif", "sans-serif"],
		},
		easing: {
			standard: "cubic-bezier(0.2, 0.0, 0, 1.0)",
			"standard-decelerate": "cubic-bezier(0, 0, 0, 1)",
			"standard-accelerate": "cubic-bezier(0.3, 0, 1, 1)",
		},
		colors: {
			secondary: "var(--md-secondary)",
			accent: {
				lighten: {
					DEFAULT: "var(--md-accent-lighten)",
					2: "var(--md-accent-lighten2)",
				},
				DEFAULT: "var(--md-accent)",
				darken: {
					DEFAULT: "var(--md-accent-darken)",
					2: "var(--md-accent-darken2)",
				},
				idle: "var(--md-accent-idle)",
				hover: {
					DEFAULT: "var(--md-accent-hover)",
					alt: "var(--md-accent-hover-alt)",
				},
				active: {
					DEFAULT: "var(--md-accent-active)",
					alt: "var(--md-accent-active-alt)",
				},
			},
			primary: {
				DEFAULT: "var(--md-primary)",
				darken: {
					DEFAULT: "var(--md-primary-darken)",
					2: "var(--md-primary-darken2)",
				},
				translucent: {
					DEFAULT: "var(--md-primary-translucent)",
					less: "var(--md-primary-translucent-less)",
				},
			},
			background: {
				DEFAULT: "var(--md-background)",
				translucent: "var(--md-background-translucent)",
				translucent2: "var(--md-background-translucent2)",
				transparent: "var(--md-background-transparent)",
			},
			shade: {
				bright: "var(--md-shade-bright)",
				less: {
					bright: "var(--md-shade-lessBright)",
					bright2: "var(--md-shade-lessBright2)",
					dark: "var(--md-shade-lessDark)",
				},
				mid: {
					DEFAULT: "var(--md-shade-mid)",
					translucent: {
						DEFAULT: "var(--md-shade-mid-translucent)",
						less: "var(--md-shade-mid-translucent-less)",
					},
					darker: "var(--md-shade-midDarker)",
				},
				dark: {
					DEFAULT: "var(--md-shade-dark)",
					less: "var(--md-shade-lessDark)",
				},
			},
			icon: {
				white: "var(--md-icon-white)",
				black: "var(--md-icon-black)",
				contrast: "var(--md-icon-contrast)",
			},
			text: {
				DEFAULT: "var(--md-text-default)",
			},
			"scrollbar-thumb": {
				DEFAULT: "var(--md-scrollbar-thumb)",
				hover: "var(--md-scrollbar-thumb-hover)",
			},
		},
	},
	shortcuts: [
		{
			btn: "inline-flex items-center justify-center bg-transparent transition-colors duration-300 text-icon-contrast",
			"btn-squircle": "btn w-10 h-10 rounded-md",
			"btn-circular": "btn w-10 h-10 rounded-full",
			"btn-text":
				"btn justify-start! h-12 px-4 rounded font-sans text-base hover:bg-shade-mid",
		},
		{
			"kbd-hint":
				"inline-flex items-center justify-center px-1 bg-accent-lighten font-sans text-sm text-shade-mid rounded",
		},
	],
});
