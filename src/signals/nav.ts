import { createSignal } from "solid-js";

export const userDrawerSignal = createSignal(true);
export const searchTextSignal = createSignal<string | null>(null);
export const searchExpansionSignal = createSignal(false);
