import { createEffect, createSignal, type Signal } from "solid-js";

/**
 * Creates a secondary signal to a {@link root} that will delay a signal
 * according to the {@link delay} parameter.
 *
 * This is useful for allowing a transition to complete before removing
 * an element from the DOM.
 */
export function useTransitionDelay<T>(root: Signal<boolean>, delay = 300) {
	const [rootAccessor, setRootAccessor] = root;
	const [transitionGap, setTransitionGap] = createSignal(rootAccessor());

	const enter = () => {
		const isVisible = rootAccessor();
		if (isVisible) {
			setTimeout(() => setTransitionGap(isVisible));
		}
	};

	const exit = () => {
		setTransitionGap(false);
		setTimeout(() => setRootAccessor(false), delay);
	};

	createEffect(enter);

	return {
		enter,
		exit,
		transitionGap,
	};
}
