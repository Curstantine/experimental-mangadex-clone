import { userDrawerSignal } from "@signals/nav";
import { useTransitionDelay } from "@signals/util";
import { Show } from "solid-js";

export default function ProfileContainer() {
	const [isUserDrawerExpanded, setUserDrawerVisibility] = userDrawerSignal;
	const { enter, exit, transitionGap } = useTransitionDelay(userDrawerSignal);

	return (
		<Show when={isUserDrawerExpanded()}>
			<div
				class="absolute inset-0 h-screen flex z-10 transform-gpu duration-300 ease-standard"
				classList={{
					"translate-x-full": !transitionGap(),
					"translate-x-0": transitionGap(),
				}}
			>
				<div class="w-full" onClick={exit} />
				<div class="flex flex-col min-w-68 overflow-y-auto bg-accent px-6 py-8">
					<UserDisplay />
					<div class="border-b border-b-solid my-4" />
					<SharedActions />
					<div class="border-b border-b-solid h-4" />
					<GuestActions />
				</div>
			</div>
			<div
				class="absolute inset-0 h-screen bg-background-translucent transition-opacity duration-300 ease-standard"
				classList={{
					"opacity-0 pointer-events-none": !transitionGap(),
					"opacity-100 pointer-events-auto": transitionGap(),
				}}
			/>
		</Show>
	);
}

const UserDisplay = () => {
	return (
		<div class="min-h-36 w-full flex flex-col items-center justify-evenly rounded text-xl font-bold hover:bg-accent-hover transition-colors duration-300 ease-standard">
			<svg
				xmlns="http://www.w3.org/2000/svg"
				fill="none"
				viewBox="0 0 24 24"
				class="h-12 w-12"
			>
				<path
					stroke="currentColor"
					stroke-linecap="round"
					stroke-linejoin="round"
					stroke-width="2"
					d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2m8-10a4 4 0 1 0 0-8 4 4 0 0 0 0 8Z"
				/>
			</svg>
			<span class="select-none">Guest</span>
		</div>
	);
};

const SharedActions = () => {
	return (
		<div class="grid grid-cols-2 items-center min-h-min">
			<a href="/settings" class="decoration-none btn-text">
				Settings
			</a>
			<button class="btn-text">Theme</button>
			<button disabled class="btn-text col-span-2">
				Interface Language
			</button>
			<button class="btn-text col-span-2">Chapter Languages</button>
			<button class="btn-text col-span-2">Content Filter</button>
		</div>
	);
};

const GuestActions = () => {
	return (
		<div class="flex flex-col h-min">
			<button class="btn-text">Sign In</button>
			<button class="btn-text">Register</button>
		</div>
	);
};
