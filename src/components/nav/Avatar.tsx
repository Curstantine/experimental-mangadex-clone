import { Show, createSignal } from "solid-js";
import { userDrawerSignal } from "src/signals/nav";

export default function Avatar() {
	const [isUserDrawerExpanded, setUserDrawerVisibility] = userDrawerSignal;
	const [isUserLoggedIn] = createSignal(false);

	const onClick = () => {
		setUserDrawerVisibility(!isUserDrawerExpanded());
	};

	return (
		<button
			class="h-10 w-10 rounded-full flex items-center justify-center bg-accent"
			onClick={onClick}
		>
			<Show
				when={!isUserLoggedIn()}
				fallback={<img src="/avatar.png" class="w-full rounded-full" />}
			>
				<svg
					xmlns="http://www.w3.org/2000/svg"
					width="24"
					height="24"
					fill="none"
					viewBox="0 0 24 24"
					class=" text-icon-contrast w-8 h-8"
				>
					<path
						stroke="currentColor"
						stroke-linecap="round"
						stroke-linejoin="round"
						stroke-width="2"
						d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2m8-10a4 4 0 1 0 0-8 4 4 0 0 0 0 8Z"
					/>
				</svg>
			</Show>
		</button>
	);
}
