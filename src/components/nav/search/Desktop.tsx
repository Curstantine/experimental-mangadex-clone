import { Show } from "solid-js";

import { searchExpansionSignal, searchTextSignal } from "@signals/nav";
import { useTransitionDelay } from "@signals/util";

import ResultCard from "./ResultCard";

export default function SearchBar() {
	const [searchText, setSearchText] = searchTextSignal;
	const [isFocused, setFocus] = searchExpansionSignal;
	const { enter, exit, transitionGap } = useTransitionDelay(searchExpansionSignal);

	return (
		<>
			<div
				class="relative flex items-center h-8 pl-4 pr-3 bg-accent rounded-md transform-gpu transition-all duration-300 ease-standard"
				outline="1 solid transparent focus-within:primary"
				classList={{
					"w-sm lg:w-3xl z-10": transitionGap(),
					"w-68": !transitionGap(),
				}}
			>
				<input
					placeholder="Search"
					class="flex-1 w-48 h-full font-sans text-base placeholder:text-text"
					onFocus={() => setFocus(true)}
				/>
				<div
					class=" mr-3 gap-1"
					classList={{ hidden: transitionGap(), flex: !transitionGap() }}
				>
					<kbd class="kbd-hint">Ctrl</kbd>
					<kbd class="kbd-hint">K</kbd>
				</div>
				<svg
					xmlns="http://www.w3.org/2000/svg"
					fill="none"
					viewBox="0 0 24 24"
					class="w-4 h-4"
				>
					<path
						stroke="currentColor"
						stroke-linecap="round"
						stroke-linejoin="round"
						stroke-width="2"
						d="M11 19a8 8 0 1 0 0-16 8 8 0 0 0 0 16Zm10 2-4.35-4.35"
					/>
				</svg>
				<Show when={isFocused()}>
					<div
						class="absolute inset-x-0 top-10 bg-background rounded-lg transform-gpu transition-transform duration-300 ease-standard origin-top-right"
						classList={{
							"scale-y-0": !transitionGap(),
							"scale-y-100": transitionGap(),
						}}
					>
						<ResultCard />
					</div>
				</Show>
			</div>
			<Show when={isFocused()}>
				<div
					class="absolute w-screen h-screen inset-0 bg-background-translucent transition-opacity duration-300 ease-standard"
					classList={{
						"opacity-0 pointer-events-none": !transitionGap(),
						"opacity-100 pointer-events-auto": transitionGap(),
					}}
					onClick={exit}
				/>
			</Show>
		</>
	);
}
