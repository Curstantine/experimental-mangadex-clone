import { Show } from "solid-js";

import { searchExpansionSignal, searchTextSignal } from "@signals/nav";
import { useTransitionDelay } from "@signals/util";

import ResultCard from "./ResultCard";

export default function SearchIcon() {
	const [isStackFocused, setStackFocus] = searchExpansionSignal;

	const showSearchBar = () => {
		setStackFocus(true);
	};

	return (
		<>
			<button
				class="flex items-center justify-center w-8 h-8 rounded-md bg-accent text-icon-contrast"
				onClick={showSearchBar}
			>
				<svg
					xmlns="http://www.w3.org/2000/svg"
					width="24"
					height="24"
					fill="none"
					viewBox="0 0 24 24"
				>
					<path
						stroke="currentColor"
						stroke-linecap="round"
						stroke-linejoin="round"
						stroke-width="2"
						d="M11 19a8 8 0 1 0 0-16 8 8 0 0 0 0 16Zm10 2-4.35-4.35"
					/>
				</svg>
			</button>
			<MobileSearchStack />
		</>
	);
}

function MobileSearchStack() {
	const [searchText, setSearchText] = searchTextSignal;
	const [isFocused, setFocus] = searchExpansionSignal;
	const { enter, exit, transitionGap } = useTransitionDelay(searchExpansionSignal);

	return (
		<Show when={isFocused()}>
			<div class="absolute inset-0 z-10">
				<div
					class="flex flex-col bg-background rounded-b-lg transform-gpu transition-transform duration-300 ease-standard"
					classList={{
						"translate-y-0": transitionGap(),
						"-translate-y-full": !transitionGap(),
					}}
				>
					<div class="flex items-center gap-2 h-14 pl-4 pr-2">
						<div
							class="flex-1 flex items-center h-8 pl-2 pr-1 rounded-lg bg-accent transition-colors duration-300 ease-standard"
							border="solid 1 transparent focus-within:primary"
						>
							<input
								value={searchText()}
								autocomplete="off"
								class="flex-1 h-full text-shade-less-bright font-sans tracking-wide"
								onInput={(e) => setSearchText(e.currentTarget.value)}
							/>
							<svg
								xmlns="http://www.w3.org/2000/svg"
								width="24"
								height="24"
								fill="none"
								viewBox="0 0 24 24"
							>
								<path
									stroke="currentColor"
									stroke-linecap="round"
									stroke-linejoin="round"
									stroke-width="2"
									d="M11 19a8 8 0 1 0 0-16 8 8 0 0 0 0 16Zm10 2-4.35-4.35"
								/>
							</svg>
						</div>
						<button class="btn-squircle h-8 w-8 hover:bg-accent" onClick={exit}>
							<svg
								xmlns="http://www.w3.org/2000/svg"
								width="24"
								height="24"
								fill="none"
								viewBox="0 0 24 24"
							>
								<path
									stroke="currentColor"
									stroke-linecap="round"
									stroke-linejoin="round"
									stroke-width="2"
									d="M18 6 6 18M6 6l12 12"
								/>
							</svg>
						</button>
					</div>
					<ResultCard />
				</div>
			</div>
			<div
				class="absolute w-screen h-screen inset-0 bg-background-translucent transition-opacity duration-300 ease-standard"
				classList={{
					"opacity-0 pointer-events-none": !transitionGap(),
					"opacity-100 pointer-events-auto": transitionGap(),
				}}
				onClick={exit}
			/>
		</Show>
	);
}
