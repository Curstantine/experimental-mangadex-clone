# Experimental MangaDex Clone

_Experimental_ reverse engineered MangaDex client written in Astro and SolidJS for the sake of performance metrics and learning.

# License

Most assets (Logo, favicon and icons) used in the project are copied from the original MangaDex website without prior permission. As such, no guarantees are made regarding the usage of these assets. If you are a MangaDex representative and would like to request the removal of these assets, please make an issue or contact me directly.

Everything else is licensed under [MIT](./LICENSE).
