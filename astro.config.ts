import { defineConfig } from "astro/config";

import prefetch from "@astrojs/prefetch";
import solidJs from "@astrojs/solid-js";
import UnoCSS from "unocss/astro";

// https://astro.build/config
export default defineConfig({
	integrations: [solidJs(), prefetch(), UnoCSS()],
	experimental: {
		assets: true,
	},
});
